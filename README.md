# Task

Add our [USPs](https://www.blacklane.com/en/cities-london#benefits-title) to the booking widget.

The *bookingWidget* (`booking-widget.pug`) already handles extra content next to the widget thru the `extraContent` property (this can be found in the `index.pug` file: the `locals` passed to the Jade file is the `bookingFormData` JS object), although it's currently limited to raw content.

The required outcome would be to have a flexible set of `road__lane` mixins inside such extra content; each `roadLane` may have either raw content, or a `media-object` content.
