/* globals describe, it */
const path = require('path')
const fs = require('fs')
const pug = require('pug')
const cheerio = require('cheerio')
const chalk = require('chalk')
const md = require('marked')

const assertions = require('./assertions')

function readSyncFile (filePath, parseJson) {
  try {
    fs.accessSync(filePath, fs.R_OK)
    const fileContent = fs.readFileSync(filePath, {
      encoding: 'utf8'
    })
    return parseJson ? JSON.parse(fileContent) : fileContent
  } catch (e) {
    console.log(chalk.yellow(`Failed to parse JSON for file ${chalk.bold(filePath)}`))
    return parseJson ? {} : ''
  }
}

function retrieveData (type, name) {
  let template
  // Check if type is part of the following whitelist
  if (['atoms', 'molecules', 'organisms', 'templates', 'pages'].indexOf(type) === -1) {
    console.log(chalk.red(`Invalid type ${chalk.bold(type)}`))
    return false
  }

  // Set the base source path
  const srcFolder = path.join(__dirname, '..', 'components', type, name)

  // Compile the pug file
  try {
    template = pug.compileFile(path.join(srcFolder, `${name}.demo.pug`), {
      cache: false,
      compileDebug: false,
      pretty: true
    })
  } catch (e) {
    console.log(chalk.red(`Empty or unavailable template named ${chalk.bold(name)}`), e)
    return
  }

  // Retrieve the use case file
  const useCases = readSyncFile(path.join(srcFolder, `${name}.demo.json`), true)
  if (!useCases) {
    console.log(chalk.red(`Use cases file for ${chalk.bold(name)} not found, or incorrectly formatted`))
    return false
  }
  if (!useCases.data || !Array.isArray(useCases.data)) {
    useCases.data = []
  }

  // Explode the use case data and return
  return {
    name: useCases._name,
    template,
    useCases: useCases.data
  }
}

function generateUseCaseContext (template, useCase) {
  // Create programmatical context based on the use case being looped
  describe(`${chalk.bold(useCase._title)} use case`, () => {
    const html = template(Object.assign({
      md
    }, useCase))
    // Load cheerio with the pug output
    const $ = cheerio.load(html)

    // Skip if no _expects array is found in the use case
    if (!Array.isArray(useCase._expects)) {
      console.log(chalk.gray(`Use cases skipped for ${chalk.bold(useCase._title)}: no expectations found`))
      return
    }

    // Programmatically create tests loop over _expects object
    useCase._expects.forEach(createTests.bind(this, $))
  })
}

function createTests ($, expectation) {
  // Get a meaningful name for the test by using the `descriptions` object exposed by assertions
  const testName = Object.keys(expectation)
    .reduce((sentence, expectationKey) => {
      const assertion = assertions[ expectationKey ] || {}
      if (typeof (assertion.description) === 'function') {
        return `${sentence} ${assertion.description(expectation)}`
      } else {
        return sentence
      }
    }, `Assert that selector ${chalk.magenta(expectation.selector)}`)

  // Programmatically create a unit test based on the looped expectation
  it(testName, () => {
    // Loop over the expectation object keys, calling the appropriate assertion function
    // They're defined in ./assertions.js
    Object.keys(expectation)
      .forEach(expectationKey => {
        const assertion = assertions[ expectationKey ] || {}
        // Just execute it if it's a function (duh)
        if (typeof (assertion.test) === 'function') {
          assertion.test($, expectation)
        }
      })
  })
}

module.exports = function generateTests (type, name) {
  const data = retrieveData(type, name)
  if (!data) return

  // generate the test context based on the component name (found in the use case file)
  describe(chalk.bold(data.name), () => {
    data.useCases
      // Filter out any use case marked as "demo only"
      .filter(data => !data._demoOnly)
      // Loop over them
      .forEach(generateUseCaseContext.bind(this, data.template))
  })
}
