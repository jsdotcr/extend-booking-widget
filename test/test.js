/* globals describe */
'use strict'

const fs = require('fs')
const path = require('path')
const chalk = require('chalk')
const generate = require('./generate')

const getAllFolders = (dir) => {
  return fs.readdirSync(dir)
    .filter((filename) => fs.statSync(`${dir}/${filename}`).isDirectory())
    .map((filename) => {
      return {
        filename,
        fullPath: `${dir}/${filename}`
      }
    })
}

getAllFolders(path.join(__dirname, '..', 'components'))
  .forEach(function (section) {
    describe(`${chalk.bold(section.filename)} section`, function () {
      getAllFolders(section.fullPath)
        .forEach(function (component) {
          generate(section.filename, component.filename)
        })
    })
  })
