const chai = require('chai')
const chalk = require('chalk')

const expect = chai.expect
chai.config.includeStack = true

exports.exists = {
  test: ($, expectation) => {
    const $selector = $(expectation.selector)
    expect($selector.length).to.equal(expectation.exists ? 1 : 0)
  },
  description: (expectation) => `${chalk.bold(`does${!expectation.exists ? ' not' : ''}`)} exist`
}

exports.matchingElements = {
  test ($, expectation) {
    const $selector = $(expectation.selector)
    expect($selector.length).to.equal(expectation.matchingElements)
  },
  description: (expectation) => `matches exactly ${chalk.bold(expectation.matchingElements)} elements`
}

exports.text = {
  test ($, expectation) {
    const $selector = $(expectation.selector)
    expect($selector.text()).to.equal(expectation.text)
  },
  description: (expectation) => `has text ${chalk.bold(expectation.text)}`
}

exports.tagName = {
  test ($, expectation) {
    const $selector = $(expectation.selector)
    expect($selector.get(0).tagName).to.equal(expectation.tagName)
  },
  description: (expectation) => `is a <${chalk.bold(expectation.tagName)}> element`
}
