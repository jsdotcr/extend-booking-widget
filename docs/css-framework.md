@page css-framework CSS framework
@parent styleguide-basics 3

# TL;DR

Zurb's [Foundation for Sites](https://foundation.zurb.com/sites/docs) CSS framework is the one used in this project.


## External constraints

Disclaimer: **there's no holy grail CSS framework (same applies to any CSS/JS/name-your-language library/framework/tool)**.

That said, the requirements to find a suitable way to handle CSS were/are:

- don't rely only on custom, internally-baked, not battle- and crossbrowser-tested style (aka: use a framework)
- framework has to be a well-known, community-backed one
- it has to handle responsive web design properly
- it plays nice with SASS and our development environment

Given these constraints, the only two viable options, at time of writing, are:
- Twitter's Bootstrap
- Zurb's Foundation for Sites


## Performance issues

Put the given restrictions aside for a moment, adopting a CSS Framework has two big cons

### Size
The size of CSS output grows. A lot. Some figures are needed here: the full *Foundation* framework is 69KB minified,
while Bootstrap is 93KB (minified as well). And this is only the CSS part of them.

What we actually need more is a proper grid system.

You don't cure your flu with antibiotics, do you?

### Semantics

Frameworks are usually great for prototyping. But it's a completely different matter if we're talking about production
code, where designers/developers actually do (or should, at least) care about the quality of the resulting code, 
including the page markup. Having lot of classes in the HTML such as `span-9` or `col-xs-12` goes in the opposite 
direction since they describe the *appearance* of an element (just like a `blue` classname, actually). Which is what CSS 
is all about.

From the W3C QA Tips:
> Good names don’t change. Think about why you want something to look a certain way, and not really about how it should 
> look. Looks can always change, but the reasons for giving something a look stay the same.
> 
> -- [http://www.w3.org/QA/Tips/goodclassnames](http://www.w3.org/QA/Tips/goodclassnames)

## Testing

How to choose between two options? Simply testing them, leaving any personal opinion out of the comparison.

First framework tested has been Bootstrap. Since v.4, they've switched to from LESS to SASS language, which made it a 
very great candidate for adoption.

In order to solve the semantic and size problems described above, only the grid mixins have been used so no external 
dependencies had to be loaded.

Bootstrap worked quite well, although it's rather uncomfortable the way it handles the markup structure: to have a grid,
a three-level nested structure is required (container, row, column). On the pro side, there's the opt-in flexbox support,
which will come handy as soon as IE9 and IE10 support will be dropped.

Foundation for Sites is written in SASS as well, so no semantic or size issues arise. Testing it out, it turned out to
solve the "issue" Bootstrap has (no container required, it's just rows and columns). Even better, it exposes a
`breakpoint` mixin which can be used to generate clean media queries in the code. It also features flexbox support, and 
it can be applied even on one single grid rather than on the whole project.


## Epilogue

Foundation turned out to be a good candidate for what we need so far. This conclusion __should not__ set in stone though:
requirements change, projects may have different scope, new frameworks/libraries will come out, Foundation may not
get any update at some point in the future, you can get hit by a bus.

Everything can, and will, change. Be ready to change your mind.
