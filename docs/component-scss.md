@page components-scss UI Components' SCSS template
@parent styleguide-basics 6

@body

# Atom

```
/**
 * @stylesheet atom-yourcomponent Your component readable name
 * @parent styleguide-atoms
 * @depends vars, atom-page, atom-transition
 *
 * Here goes a nice description of your component. Please mind the token following that `@stylesheet` before as that's the one that may be used to link this page on other files. That token is always prefixed with either `atom` (it includes the ones in the shared folder), `molecule` or `organism`, depending which type of component you're building.
 * @parent is the reference to a group of pages. You can find the list on [styleguide].
 * @depends is the list of dependencies imported by the file. All the files have `vars` and `atom-page`.
 * @iframe below have the references to the use case demos. They always follow the form `demos/$categoryName/$componentName.demo.html` and `demos/$categoryName/$componentName.html`
 *
 * @iframe demos/atoms/yourcomponent.demo.html
 * @iframe demos/atoms/yourcomponent.html
 */
/**
  * Imports are split in two different sections. First come the `vars` and `foundation` (must-haves).
  */
@@import "../../shared/vars";
@@import "foundation";

/**
  * Then there's the rest. They're ordered by category first (shared, then atoms, then modifiers) and alphabetically after that.
  * `page` is still a must-have, but has to come after vars and foundation, all the rest is optional. 
  * `typography` is included by `page` as well, but it's specified whenever a specific typo has to be used in the file (e.g.: the @header or @typography mixin). This way allows to build the component CSS file independently. 
  * Although common, `modifiers` and `form` are not included by default in the page, thus should be included if used (e.g.: if you're dealing with a form, please include `atoms/form/form`, even though it'll most likely be included by some other files too). 
  */
@@import "../../shared/modifiers";
@@import "../../shared/page";
@@import "../../shared/typography";
@@import "../../atoms/form/form";

@@mixin link-active {
  background-color: $color-accented-inverted;
  color: $color-text;
}

/**
  * Includes come first, so they may get overridden if necessary;
  * Media queries, pseudo states and classes come after
  * Nested elements (elements and modifiers) are the last ones
  */
a {
  @@include atom-transition;

  color: $color-links;

  &:hover {
    color: $color-accented-hover;
  }
}

```
