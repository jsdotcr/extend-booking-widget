@page custom-tags How to use custom documentation tags
@parent styleguide-howtos 1

# Custom tags
The default documentjs/documentcss solution has been extended with the following custom tags:

## @bem

It has the same [@stylesheet](http://documentjs.com/docs/documentjs.tags.stylesheet.html)'s behaviour and signature, although it adds a clearer reference to a BEM/Atomic design module.
Its reference name (e.g.: the first parameter) should be in the form: `{atom,molecule,organism}-<name>`.

## @bemblock, @bemelement, @bemmodifier

They refer to the basic BEM elements. The reference name is inferred directly from the class name (`.bem-block__element--modifier` may be referred as `bem-block-element-modifier`).
It creates a new section, adding by default the full class name used by the element.


## @sassvar

It's used to document SASS-like variables, including maps. It automatically adds the variable/map values to the documentation as well.
If a colour is matched, a fancy coloured box is created.

For instance:

`@sassvar Variable name`

Its reference name is set to the variable name, without the $ sign.


## @depends
It works a bit like [@inherits](http://documentjs.com/docs/documentjs.tags.inherits.html), but it expects a list of comma-separated dependencies.

For instance:

`@depends vars, atom-page, molecule-media-object`
