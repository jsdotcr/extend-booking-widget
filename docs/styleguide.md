@page styleguide Apollo: Blacklane's design system
@group styleguide-basics 0 Basics
@group styleguide-howtos 1 How tos
@group styleguide-projects 2 Projects
@group styleguide-shared 3 Shared
@group styleguide-atoms 4 Atoms
@group styleguide-molecules 5 Molecules
@group styleguide-organisms 6 Organisms

# Getting started

If I were you, I'd start to getting used to the [terminology] used all around the repo. It's a good thing that we all speak the very same language. Although, it's a very fluid language: if requested and agreed by the team, things can change tomorrow.

## Run

To see the design system in action you may want to run the command:
```
npm start
```
This will build all the necessary static files, including the distribution-ready styles. To access the documentation,
head your browser to [http://localhost:9876/](http://localhost:9876/).

Any change done on the source SASS, markdown, template, data files will trigger an automagic content refresh.

Enjoy.


## Tools

This living design system uses some nice tools under the hood, most notably [gulp](https://gulpjs.com) and [documentjs](http://documentjs.com).


### DocumentJS

This is the _actual_ style guide generator. While its main use case is to document Javascript code, it features some useful tags so CSS can be documented as well. It's template-agnostic, documentation is within the code, it's written in NodeJS and it can be easily extended.

The [documentjs.json](documentjs.json) file is its starting point where the main paths are defined. Its style can be overridden playing with the less files available in the [styleguide/styles](styleguide/styles) folder.

## Styles

The current structure is based on Brad Frost's [Atomic Design](https://bradfrost.com/blog/post/atomic-web-design), which is a methodology for creating [design systems](http://atomicdesign.bradfrost.com/chapter-1/).

[atomic-design] is a good starting point to get used to its core concepts and its terminology.


## Views and demos

Jade templates mimic the atomic design folder architecture, borrowing the very same concepts of building blocks, and they're available in the [`views`](views) folder.
They may be used by either any dependent project and the design system demos; when it comes to Apollo, it processes that directory to collect all the templates, which are then wrapped in an [HTML file container](styleguide/views/wrapper.html), and thus used to generate the final demo files, one for each of the [components-use-cases] found.

All the resulting demos are static HTML files then available in the `public/demos` folder. The resulting files may be linked in any stylesheet using the [@iframe tag](http://documentcss.com/docs/lsg-quickstart-demos.html).


## How to document stylesheets

You can either add JSDoc-like comments withing the SASS files directly, or even add some more descriptive Markdown
files.

Either ways, you may want to take a look at the [DocumentJS tags](http://documentjs.com/docs/documentjs.tags.html)
and/or at [DocumentJS' docs](http://documentjs.com/docs/index.html). There are also some useful guides available on
[DocumentCSS.com](http://documentcss.com/docs/lsg-quickstart.html).
Some custom tags are also defined, and may be used to document the stylesheets as well. More informations are available in [custom-tags].
