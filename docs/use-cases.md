@page use-cases How to create use cases
@parent styleguide-howtos 2

The main problem of having a style guide (or code documentation, in general) is to keep it up to date with the codebase, which may become an issue if in a hurry, or you simply forget to update it. Having a living design system may be a way to prevent this: it autogenerates documentation whenever the codebase is changed.

This is what happens on apollo, since the documentation is generated based on the production-ready mixins, combined with style and some mocked data (that is, each use case).

The relevant use cases should be defined in order to generate a decent and well-formatted documentation, plus providing tests for the component itself.


## Filesystem structure

A `json` file needs to be added as a template's sibling, with its very same file name, so the component's markup structure look like:

```
apollo
└─┬ components
  └─┬ component_category
    └─┬ component_name
      ├── component_name.jade
      ├── component_name.mixins.jade
      └── component_name.json
```

## Use case file

A use case file should look like this:

```json
{
  "_name": "Fancy component name",
  "data": [
    {
      "_title": "#1 Use case name",
      "_demoOnly": false,
      "_testOnly": false,
      "_expects": [
        {
          "selector": ".component_name__element",
          "exists": false
        },
        {
          "selector": ".component_name__element--modifier-anything",
          "text": "you want"
        },
        {
          "selector": ".component_name--modifier",
          "exists": true
        }
      ],
      "anything": "you want",
      "that": [ "is", "then", "used", "by", "the", "template" ]
    },
    {
      "_title": "#2 Use case name",
      "_demoOnly": true,
      "_testOnly": false,
      "again": "Anything can be used here! It's",
      "gonna": {
        "be": [ "the", "same" ],
        "as": "rendering the",
        "jade": "template"
      }
    }
  ]
}
```


That creates two different use cases which are then reflected in the demo HTML output. The effect is the same as rendering the jade template doing:

```js
var jade = require('jade')

var html = jade.render('panorama.jade', {
  anything: 'you want',
  that: [ 'is', 'then', 'used', 'by', 'the', 'template' ]
})
html += jade.render('panorama.jade', {
  again: 'Anything can be used here. It\'s',
  gonna: {
    be: [ 'the', 'same'],
    as: 'rendering the',
    jade: 'template'
  }
})
```

All the data passed to the template is wrapped in the `data` object. This can either be an array of objects, or just an object: each element represents a use case passed to the template as locals object.


### Non-template data

Please refrain from using any key that starts with `_` as these are used to either run the tests, or to provide more informations to the documentation build task, because we want to have a prettier documentation website (it's a design system, after all, not Javadoc).


### Root-level data

The use case file *should always* have two main objects:

* `_name`: the component name, in a human-readable form
* `data`: the collection of use cases (or just one single use case... although: are you sure about it? Just one? Aren't you missing anything?)


### Use case-level data

* `_title`: the use case name, in a human-readable form
* `_demoOnly`: boolean, default is `false`. When it is defined and true, tests will be skipped for this component (even if `_expects` has something in it)
* `_testOnly`: boolean, default is `false`. When it is defined and true, the demo won't be generated for this use case
* `_expects`: array of *expectations* (more about this in [testing])
