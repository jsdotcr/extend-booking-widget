@page components UI Components
@parent styleguide-basics 5

The folder `components` contains the main library code for the UI components of our front-end.
Project structure follows the principles of [atomic-design] and HTML markup(jade mixins), styles(scss mixins) and JS code is grouped by concern.


## Where to put your code?

You're probably building a standalone component, grouping a set of components or writing some shared logic. In any case you want to all of the related files in one single folder whether they are html, css or js files.

### Atoms
They represent a single element, something that doesn't make sense to further divide. Like a button, link, title, input, etc.

### Molecules
A group of atoms semantically related with a single purpose.

### Organisms
A group of molecules and atoms that relate to each other but can serve different(still related) purposes. They are the building blocks of a page.

### Shared
Outside of the atomic design, is a place to put helper methods, shared logic for code reuse, and stuff that doesn't represent anything tangible by itself.

## Folder structure example
```
components/
-- atoms/
---- search-button/
------ search-button.scss
------ search-button.html
---- search-input/
------ search-input.js
------ search-input.scss
------ search-input.html
-- molecules/
---- search-bar/ <-- uses the atoms
------ search-bar.js
------ search-bar.scss
------ search-bar.html
-- organisms/
---- fancy-search/ <-- groups other molecules and atoms
------ fancy-search.js
------ fancy-search.html
------ fancy-search.scss
-- shared/
---- style-reset.scss <-- used by several components
```

## How to export it
The `index*` files on the root of this folder require all of the pieces that will be exported for others to use.
