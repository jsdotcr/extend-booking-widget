@page testing How to test components
@parent styleguide-howtos 3

Tests are run by [mocha](https://mochajs.org), using [chai](http://chaijs.com/api/bdd/)'s BDD syntax (`expect`).

They get automagically generated starting from the component's use cases (more info at [use-cases]) and they can be run using the npm script `npm test`.


# What

The tests refer to the component's final HTML output, so we ensure that for a given input (a use case), the output will be the one described by the relative expectations.

# How

## Run

By running `npm test`, the expectations generator will fetch all the defined use case examples, generating mocha/chai expectations.


## Add

New expectations can be added by creating an `_expects` object within the single use case. So the json file would look something like:

```json
{
  "_name": "O2 Oxygen",
  "data": [
    {
      "_title": "Atoms count",
      "_expects": [
        {
          "selector": ".o2-oxygen__atom-count",
          "exists": true
        },
        {
          "selector": ".o2-oxygen__atom-count",
          "text": 2
        }
      ],
      "count": 2
    },
    {
      // Yet another use case
    }
  ]
}
```


# Expectation

An expectation is an object with two or more key-value pairs, one **reference selector** and one or more **expectation types**.


## `selector`

As the name might suggest, this is a regular CSS selector. It is the DOM element we're describing.


## Expectation types

The following keys can be used in an expectation object:

### `exists`

It checks for the selector's (non)existence.
A `true` value will check whether the selector matches exactly one element, while if it's `false` the matching elements have to be zero to pass.


### `matchingElements`

It is an extended version of `exists`; the expectation ensures that the CSS selector matches the number of elements specified as matchingElements value.


### `text`

This would ensure that the text provided as a value matches the element content.


### `tagName`

The expectation ensures that the element matched by the selector is of the type stated as tagName value.


### Add new expectation types

This can be done by exporting a new function in `test/assertions.js`. [cheerio](https://github.com/cheeriojs/cheerio#selectors) is used to handle the DOM, so you might wanna take a look at how it can help you out.

A description can be added as well, so mocha logs will be useful. Your new expectation type description will be appended to the sentence *Assert that selector $SELECTOR$*.
