@page terminology Terminology
@parent styleguide-basics 1


> _It's necessary to find the right words: words are important!_
> 
> -- [Nanni Moretti](https://www.youtube.com/watch?v=qtP3FWRo6Ow) 

# Global terms

## Design system

A design system is a guide that provides a reference to developers on how the interface is intended to look, and helps designers to be consistent as the time, and the refactoring, will go on so to always have a consistent UI.

More about this in [Jina Bolton's A List Apart article](http://alistapart.com/article/writingainterfacestyleguide). 


## Pattern library

This term might be used at some point. Although it has a slightly different meaning, for us is the same as saying *Design system*.


## Living design system

Having a design system is ok. But it's not enough to keep it consistent over time. It should be able to autogenerate itself as soon as changes are made to the codebase.

On top of it, a **Single Source of Truth** becomes necessary to fully achieve this goal. This means avoiding:

* *template duplication*: duplicate the markup in the SASS documentation, e.g.: to show how to use it, will lead to have two different files to maintain. For each component. Fail.
* *static HTML output*: having the final HTML output in the documentation leads people to simply copy&paste it. Easy action indeed, but the drawback is to break the link between the design system and the project using it, meaning yet another thing to maintain separately.

More about this in [Ian Feather's A Maintanable Style Guide](http://engineering.lonelyplanet.com/2014/05/18/a-maintainable-styleguide.html)


## Atomic design

It's how Apollo is structured, using chemistry-like words so to have a well-organised building block structure.

More about this in [atomic-design].


## BEM

> meaning block, element, modifier – is a front-end naming methodology thought up by the guys at Yandex. It is a smart way of naming your CSS classes to give them more transparency and meaning to other developers. They are far more strict and informative, which makes the BEM naming convention ideal for teams of developers on larger projects that might last a while.
>
> -- [Harry Roberts's blog](http://csswizardry.com/2013/01/mindbemding-getting-your-head-round-bem-syntax/)


# Specific terms

## Component

Most of the times this term refers to any of our atoms, molecules or organisms. Sometimes, though, it might be used to define organisms only (e.g.: when the context is a project using apollo, atoms and molecules aren't really part of that game, so component X === organism X). If not sure, just ask :)


## Use cases

Use cases are JSON objects. They are used to:
* Create each component's demo(s)
* Add metadata used by the documentation so it might look nicer
* Provide test expectations 

A collection of use cases referring to the very same component is stored in a `$COMPONENT_DIR$/$COMPONENT_NAME$.demo.json` file.

More about this in [use-cases].


## Demos

Demos are static HTML files created by the building tool that merges the component's Jade markup file with each of its use cases.


## Use case examples, expectations

The words/concept come from the BDD world and they're used to test the Jade templates and their output as DOM.

More on this in [testing].
