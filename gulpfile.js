const gulp = require('gulp')

const demosTask = require('./tasks/demos')

gulp.task('demos', ['sass-demos'], demosTask)
