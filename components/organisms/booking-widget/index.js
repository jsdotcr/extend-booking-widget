/* eslint-env browser */
import { BookingForm, Tabs } from '../../molecules'

const tabsSelector = '.tabs'
const onewayFormSelector = 'form#oneway'
const hourlyFormSelector = 'form#hourly'
const selectedFormClass = 'tabs__content--active'

/**
 * BookingWidget
 * groups and initializes its underling components
 *
 * @property {Tabs} tabs
 * @property {Object} forms
 */
export class BookingWidget {
  /**
   * @param {HTMLElement} widget
   * @param {Atlas} atlas
   * @listens Tabs#tab-changed
   */
  constructor (widget, atlas) {
    const tabs = widget.querySelector(tabsSelector)
    const onewayForm = widget.querySelector(onewayFormSelector)
    const hourlyForm = widget.querySelector(hourlyFormSelector)
    let selectedForm

    this.forms = {
      get selected () { return selectedForm },
      set selected (form) {
        if (!(form instanceof BookingForm)) {
          throw TypeError()
        }
        if (this.hasOwnProperty(form.type) && form !== selectedForm) {
          if (selectedForm) {
            selectedForm.classList.remove(selectedFormClass)
          }
          selectedForm = form
          selectedForm.classList.add(selectedFormClass)
        }
      }
    }

    if (tabs) {
      this.tabs = new Tabs(tabs)
    }
    if (onewayForm) {
      this.forms.oneway = new BookingForm(onewayForm, atlas, 'oneway')
    }
    if (hourlyForm) {
      this.forms.hourly = new BookingForm(hourlyForm, atlas, 'hourly')
    }

    // default selected form
    this.forms.selected = this.forms.oneway ? this.forms.oneway : this.forms.hourly

    // overide selection if matching URL hash is present
    const hash = location.hash.replace(/^#/, '')
    if (hash === 'oneway' || hash === 'hourly') {
      this.tabs.selected = hash === 'oneway'
        ? this.tabs.get(0)
        : this.tabs.get(1)
      this.forms.selected = this.forms[ hash ]
    }

    this.tabs.on('tab-changed', (_, index) => {
      this.forms.selected = index === 0
        ? this.forms.oneway
        : this.forms.hourly

      return this.forms.selected
    })
  }
}
