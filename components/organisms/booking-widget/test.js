/* eslint-env browser, mocha */
import { BookingWidget } from '.'
import { expect } from 'chai'
import { html } from '../../shared/tags'
import { fakeAtlas } from '../../shared/mocks'

const testWidget = () => html`
<div>
  <div class="tabs">
    <div class="tabs__item oneway"></div>
    <div class="tabs__item hourly"></div>
  </div>
  <div class="tabs__container">
    <form id="oneway"></form>
    <form id="hourly"></form>
  </div>
</div>
`

describe('BookingWidget', () => {
  let widgetElement, widget, hourlyTab, onewayTab

  beforeEach(() => {
    widgetElement = testWidget()
    widget = new BookingWidget(widgetElement, fakeAtlas())
    onewayTab = widgetElement.querySelector('.tabs :first-child')
    hourlyTab = widgetElement.querySelector('.tabs :last-child')
  })

  it('should have a tabs property', () => {
    expect(widget).to.have.a.property('tabs')
  })

  it('should have a forms property', () => {
    expect(widget).to.have.a.property('forms')
  })

  describe('forms', () => {
    it('should have a selected property', () => {
      expect(widget.forms).to.have.a.property('selected')
    })

    it('should have a hourly property', () => {
      expect(widget.forms).to.have.a.property('hourly')
    })

    it('should have a oneway property', () => {
      expect(widget.forms).to.have.a.property('oneway')
    })
  })

  it('should select oneway tab and form by default', () => {
    expect(widget.tabs.selected).to.equal(onewayTab)
    expect(widget.forms.selected.type).to.equal('oneway')
  })

  it('should select the right tab according to the URL hash', () => {
    location.hash = 'hourly'
    widget = new BookingWidget(widgetElement, fakeAtlas())
    expect(widget.tabs.selected).to.equal(hourlyTab)
    expect(widget.forms.selected.type).to.equal('hourly')
    location.hash = 'oneway'
    widget = new BookingWidget(widgetElement, fakeAtlas())
    expect(widget.tabs.selected).to.equal(onewayTab)
    expect(widget.forms.selected.type).to.equal('oneway')
  })

  it('should select a tab and its corresponding form when the tab is clicked', () => {
    expect(widget.tabs.selected).to.equal(onewayTab)
    expect(widget.forms.selected.type).to.equal('oneway')
    hourlyTab.dispatchEvent(new CustomEvent('click', { bubbles: true }))
    expect(widget.tabs.selected).to.equal(hourlyTab)
    expect(widget.forms.selected.type).to.equal('hourly')
  })
})
