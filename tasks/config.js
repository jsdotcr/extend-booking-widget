const browserSync = require('browser-sync').create()
const gutil = require('gulp-util')
const path = require('path')

const { magenta, red, white } = gutil.colors.bold
// template tag for removing leading spaces or tabs after new lines
const stripLeadingSpaces = (parts, ...values) => parts.map((part, i) =>
    part.replace(/\n\s*/g, '\n') + (i < values.length ? values[i] : '')
  ).join('')

module.exports = {
  browserSync,
  destination: path.join(__dirname, '..', 'public'),
  errorHandler (e) {
    gutil.log(stripLeadingSpaces`
      ${magenta`***********************************************************************`}
      Hey there. It's gulp ${white`sass`} task here
      We've got this error on the pipeline:
      ${red(e.message)}
      ${e.file || e.path ? `At file ${white(e.file || e.path)}, line ${e.line}` : ''}
      ${e.plugin ? `Plug-in used: ${white(e.plugin)}` : ''}
      ${e.stack ? `Stacktrace:\n ${white(e.stack)}` : ''}

      Cheers,
      ${white`Plumber`}
      ${magenta`***********************************************************************`}
    `)
  }
}
