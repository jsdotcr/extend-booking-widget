const gulp = require('gulp')
const { destination, browserSync, errorHandler } = require('./config')

const data = require('gulp-data')
const fs = require('fs')
const pug = require('gulp-pug')
const md = require('marked')
const path = require('path')
const plumber = require('gulp-plumber')
const wrap = require('gulp-wrap')

const mixinRegExp = new RegExp('^mixin (.*)$', 'gm')
const sourceTemplates = '{atoms,molecules,organisms}/**/*.pug'
const useCaseWrapper = 'styleguide/views/loop.pug'
const demoWrapper = 'styleguide/views/wrapper.html'
const destFolder = path.join(destination, 'demos')
const readSyncFile = (filePath, parseJson = false) => {
  let fileContent
  try {
    fs.accessSync(filePath, fs.R_OK)
    fileContent = fs.readFileSync(filePath, {
      encoding: 'utf8'
    })
    return parseJson ? JSON.parse(fileContent) : fileContent
  } catch (e) {
    if (fileContent && fileContent.length > 0) {
      throw new Error(`${filePath} exists but it has no valid JSON.`)
    }
    return parseJson ? {} : ''
  }
}
const staticJadeLocals = {
  md,
  assetsPrefix: ''
}

module.exports = () =>
  /**
   * Available within `file.data` in wrapper.html (that is, after Jade template has been transformed):
   * - _rawTemplate: Jade template
   * - _mixins: array of mixins used by the main Jade file
   * - _style: SASS code
   * - Any content within the related use cases/JSON file (e.g.: _name and data)
   * - Anything within `staticJadeLocals` object above
   *
   * All this stuff is also exposed to the Jade files as locals (so you can actually use `md` to convert your
   * markdown string to real HMTL).
   */
  gulp.src(sourceTemplates)
  // Wrap errors in the chain using plumber
  .pipe(plumber(errorHandler))
  // Get use cases to use as template data
  .pipe(
    data((file) => {
      const templateData = readSyncFile(file.path.replace(/\.pug$/, '.json'), true)
      // _rawTemplate is then used by the HTML demo wrapper to output the template content
      templateData._rawTemplate = file.contents.toString().trim()
      /* eslint-disable no-unused-vars */
      const [ _, ...mixinMatches ] = mixinRegExp.exec(templateData._rawTemplate) || []
      /* eslint-enable no-unused-vars */
      templateData._mixins = mixinMatches
      return Object.assign({}, staticJadeLocals, templateData)
    })
  )
  .pipe(
    data((file) => {
      if (file.data._mixins.length > 0) {
        return file.data
      }
      const fileName = path.join(
        destFolder,
        path.basename(file.path, '.demo.pug')
      ) + '.css'
      const styleData = readSyncFile(fileName)

      return Object.assign({}, file.data, {
        _style: styleData.replace(/"/g, '\\"').replace(/'/g, '\\\'').replace(/\n/g, '\\\n')
      })
    })
  )
  // Allow the original template to be looped thru the use cases defined by the json file loaded at the step before
  .pipe(wrap({
    src: useCaseWrapper
  }))
  // Convert the wrapped template in real HTML
  .pipe(pug({
    basedir: __dirname,
    cache: false,
    compileDebug: true,
    pretty: true
  }))
  // Add some whistles and bells around the generated HTML (eg.: the demo layout, code prettifier, demo-specific CSS)
  .pipe(wrap({
    src: demoWrapper
  }))
  // Save the generated HTML files
  .pipe(gulp.dest(destFolder))
  // Reload browserSync
  .pipe(
    browserSync.stream({
      match: '**/*.html',
      once: true
    })
  )
